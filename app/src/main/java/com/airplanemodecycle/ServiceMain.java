package com.airplanemodecycle;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ServiceMain extends Service {

    private static final int NOTIFICATION_ID = 1;
    private static final int AIR_MODE = 1;
    private static final int NO_AIR_MODE = 2;

    private boolean allowAirplaneMode = false;

    private int mode = AIR_MODE;
    private int secondsAirModeCount = 0;
    private int secondsNoAirModeCount = 0;

    private ScheduledFuture<?> scheduledFuture;

    private Preferences preferences;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Logcat.d(intent.getAction());
            switch (intent.getAction()) {
                case Intent.ACTION_SCREEN_ON:
                    break;
                case Intent.ACTION_SCREEN_OFF:
                    allowAirplaneMode = true;
                    break;
                case Intent.ACTION_USER_PRESENT:
                    allowAirplaneMode = false;
                    AirplaneModeUtil.turnOff(getApplicationContext());
                    break;
                default:
                    break;
            }
        }
    };

    public void onCreate() {
        super.onCreate();
        preferences = new Preferences(getApplicationContext());
        IntentFilter lockFilter = new IntentFilter();
        lockFilter.addAction(Intent.ACTION_SCREEN_ON);
        lockFilter.addAction(Intent.ACTION_SCREEN_OFF);
        lockFilter.addAction(Intent.ACTION_USER_PRESENT);
        registerReceiver(receiver, lockFilter);
        showNotification();
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, ServiceMain.class);
    }

    public static boolean isRunning(Context context) {
        return ComponentUtil.isServiceRunning(context, ServiceMain.class);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
            scheduledFuture = null;
        }
        ScheduledExecutorService timer = Executors.newScheduledThreadPool(1);
        scheduledFuture = timer.scheduleWithFixedDelay(new Runnable() {

            @Override
            public void run() {
                Logcat.i("Timer 1 second iteration...");
                if (!allowAirplaneMode) {
                    return;
                }
                Logcat.d("Allows to work... Mode " + mode);
                int prefSecondsAirMode = preferences.getSecondsAirMode();
                int prefSecondsNoAirMode = preferences.getSecondsNoAirMode();
                Logcat.d("Count air mode " + secondsAirModeCount + " from " + prefSecondsAirMode);
                Logcat.d("Count air mode " + secondsNoAirModeCount + " from " + prefSecondsNoAirMode);
                if (prefSecondsAirMode <= 0) {
                    AirplaneModeUtil.turnOff(getApplicationContext());
                    return;
                }
                if (prefSecondsNoAirMode <= 0) {
                    AirplaneModeUtil.turnOn(getApplicationContext());
                    return;
                }
                switch (mode) {
                    case AIR_MODE:
                        if (secondsAirModeCount >= prefSecondsAirMode - 1) {
                            mode = NO_AIR_MODE;
                            secondsAirModeCount = 0;
                            secondsNoAirModeCount = 0;
                        } else {
                            if (secondsAirModeCount == 0) {
                                AirplaneModeUtil.turnOn(getApplicationContext());
                            }
                            secondsAirModeCount++;
                        }
                        break;
                    case NO_AIR_MODE:
                        if (secondsNoAirModeCount >= prefSecondsNoAirMode - 1) {
                            mode = AIR_MODE;
                            secondsAirModeCount = 0;
                            secondsNoAirModeCount = 0;
                        } else {
                            if (secondsNoAirModeCount == 0) {
                                AirplaneModeUtil.turnOff(getApplicationContext());
                            }
                            secondsNoAirModeCount++;
                        }
                        break;
                    default:
                        break;
                }
            }
        }, 0, 1, TimeUnit.SECONDS);
        return START_STICKY;
    }

    private void showNotification() {
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new NotificationCompat.Builder(getApplicationContext())
                .setContentIntent(pendingIntent).build();
        startForeground(NOTIFICATION_ID, notification);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
        }
        stopForeground(true);
        stopSelf();
    }
}
