package com.airplanemodecycle;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

public class ActivityMain extends AppCompatActivity
        implements CompoundButton.OnCheckedChangeListener, TextWatcher,
        DialogInterface.OnClickListener {

    private TextView appText;
    private SwitchCompat appSwitch;
    private EditText secondsAirModeValue;
    private EditText secondsNoAirModeValue;

    private Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.mainLayout).requestFocus();
        appText = (TextView) findViewById(R.id.app_text);
        appSwitch = (SwitchCompat) findViewById(R.id.app_switch);
        appSwitch.setChecked(ServiceMain.isRunning(getApplicationContext()));
        appSwitch.setOnCheckedChangeListener(this);
        secondsAirModeValue = (EditText) findViewById(R.id.seconds_air_mode_value);
        secondsNoAirModeValue = (EditText) findViewById(R.id.seconds_no_air_mode_value);

        preferences = new Preferences(getApplicationContext());

        setCaptions();
        secondsAirModeValue.setText(String.valueOf(preferences.getSecondsAirMode()));
        secondsNoAirModeValue.setText(String.valueOf(preferences.getSecondsNoAirMode()));
        secondsAirModeValue.addTextChangedListener(this);
        secondsNoAirModeValue.addTextChangedListener(this);
    }

    private void toggleApp() {
        if (appSwitch.isChecked() && !ServiceMain.isRunning(getApplicationContext())) {
            startService(ServiceMain.getStartIntent(getApplicationContext()));
        } else if (!appSwitch.isChecked() && ServiceMain.isRunning(getApplicationContext())) {
            stopService(ServiceMain.getStartIntent(getApplicationContext()));
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b && !manageWriteSettingsPermission()) {
            appSwitch.setChecked(false);
            return;
        }
        setCaptions();
        toggleApp();
    }

    private void setCaptions() {
        appText.setText(getString(R.string.app, getResources().getStringArray(R.array.app_state)
                [!appSwitch.isChecked() ? 0 : 1]));
    }

    @Override
    public void afterTextChanged(Editable s) {
        savePreferences();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start,
                                  int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start,
                              int before, int count) {}

    @Override
    public void onStop() {
        super.onStop();
        savePreferences();
    }

    private void savePreferences() {
        try {
            String secondsAirMode = secondsAirModeValue.getText()
                    .toString();
            String secondsNoAirMode = secondsNoAirModeValue.getText()
                    .toString();
            if (!secondsAirMode.isEmpty()) {
                preferences.setSecondsAirMode(Integer.parseInt(secondsAirMode));
            }
            if (!secondsNoAirMode.isEmpty()) {
                preferences.setSecondsNoAirMode(Integer.parseInt(secondsNoAirMode));
            }
        } catch (NumberFormatException e) {
            Logcat.e(e.getMessage());
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which){
            case DialogInterface.BUTTON_POSITIVE:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                }
                break;
            default:
                break;
        }
        dialog.dismiss();
    }

    private boolean manageWriteSettingsPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                !Settings.System.canWrite(getApplicationContext())) {
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .create();
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                    getString(android.R.string.cancel), this);
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                    getString(android.R.string.ok), this);
            alertDialog.setMessage(getString(R.string.prompt_settings));
            alertDialog.show();
            return false;
        }
        return true;
    }
}
