package com.airplanemodecycle;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {

	private SharedPreferences preferences;

	private static final String SECONDS_AIR_MODE = "secondsAirMode";
	private static final String SECONDS_NO_AIR_MODE = "secondsNoAirMode";

	public Preferences(Context context)
	{
		preferences = PreferenceManager.getDefaultSharedPreferences(context);
	}


	public void clearPreferences()
	{
		preferences.edit().clear().apply();
	}


	public int getSecondsAirMode()
	{
		return preferences.getInt(SECONDS_AIR_MODE, 0);
	}


	public void setSecondsAirMode(int value)
	{
		preferences.edit().putInt(SECONDS_AIR_MODE, value).apply();
	}

	public int getSecondsNoAirMode()
	{
		return preferences.getInt(SECONDS_NO_AIR_MODE, 0);
	}


	public void setSecondsNoAirMode(int value)
	{
		preferences.edit().putInt(SECONDS_NO_AIR_MODE, value).apply();
	}
}
