package com.airplanemodecycle;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

public final class AirplaneModeUtil {

    public static boolean isEnabled(Context context) {
        return Settings.System.getInt(context.getContentResolver(),
            Settings.Global.AIRPLANE_MODE_ON, 0) == 1;
    }

    public static void turnOn(Context context) {
        if (!isEnabled(context)) {
            toggle(context);
        }
    }

    public static void turnOff(Context context) {
        if (isEnabled(context)) {
            toggle(context);
        }
    }

    public static void toggle(Context context) {
        boolean isEnabled = isEnabled(context);
        // toggle airplane mode
        Settings.System.putInt(context.getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, isEnabled ? 0 : 1);
        // Post an intent to reload
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", !isEnabled);
        context.sendBroadcast(intent);
    }
}
